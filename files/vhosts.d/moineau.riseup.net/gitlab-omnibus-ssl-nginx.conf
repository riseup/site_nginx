## GitLab
##
## Modified from nginx http version
## Modified from http://blog.phusion.nl/2012/04/21/tutorial-setting-up-gitlab-on-debian-6/
## Modified from https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html
##
## Lines starting with two hashes (##) are comments with information.
## Lines starting with one hash (#) are configuration parameters that can be uncommented.
##
##################################
##        CONTRIBUTING          ##
##################################
##
## If you change this file in a Merge Request, please also create
## a Merge Request on https://gitlab.com/gitlab-org/omnibus-gitlab/merge_requests
##
###################################
##         configuration         ##
###################################
##
## See installation.md#using-https for additional HTTPS configuration details.

upstream gitlab-workhorse {
  server unix:/var/opt/gitlab/gitlab-workhorse/socket fail_timeout=0;
}

## Redirects all HTTP traffic to the HTTPS host
server {
  ## Either remove "default_server" from the listen line below,
  ## or delete the /etc/nginx/sites-enabled/default file. This will cause gitlab
  ## to be served if you visit any address that your server responds to, eg.
  ## the ip address of the server (http://x.x.x.x/)
  listen 0.0.0.0:80;
  server_name bugs.otr.im moineau.otr.im; ## Replace this with something like gitlab.example.com
  server_tokens off; ## Don't show the nginx version number, a security best practice
  return 301 https://$http_host$request_uri;
  access_log  /var/log/nginx/gitlab_access.log;
  error_log   /var/log/nginx/gitlab_error.log;
}

## HTTPS host
server {
  listen 0.0.0.0:443 ssl;
  server_name bugs.otr.im moineau.otr.im; ## Replace this with something like gitlab.example.com
  server_tokens off; ## Don't show the nginx version number, a security best practice
  root /opt/gitlab/embedded/service/gitlab-rails/public;

  ## Strong SSL Security
  ## https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html & https://cipherli.st/
  ssl on;
  ssl_certificate /etc/x509/certs/bugs.otr.im-full.pem;
  ssl_certificate_key /etc/x509/keys/bugs.otr.im.key;

  # GitLab needs backwards compatible ciphers to retain compatibility with Java IDEs
  ssl_ciphers "ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4";
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout 5m;

  ## See app/controllers/application_controller.rb for headers set

  ## [Optional] If your certficate has OCSP, enable OCSP stapling to reduce the overhead and latency of running SSL.
  ## Replace with your ssl_trusted_certificate. For more info see:
  ## - https://medium.com/devops-programming/4445f4862461
  ## - https://www.ruby-forum.com/topic/4419319
  ## - https://www.digitalocean.com/community/tutorials/how-to-configure-ocsp-stapling-on-apache-and-nginx
  # ssl_stapling on;
  # ssl_stapling_verify on;
  # ssl_trusted_certificate /etc/nginx/ssl/stapling.trusted.crt;
  # resolver 208.67.222.222 208.67.222.220 valid=300s; # Can change to your DNS resolver if desired
  # resolver_timeout 5s;

  ## [Optional] Generate a stronger DHE parameter:
  ##   sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 4096
  ##
  # ssl_dhparam /etc/ssl/certs/dhparam.pem;

  ## Individual nginx logs for this GitLab vhost
  access_log  /var/log/nginx/gitlab_access.log;
  error_log   /var/log/nginx/gitlab_error.log;

#  rewrite ^(/download/.*)/media/(.*)\..*$ $1/mp3/$2.mp3 last;
#  rewrite ^/git/libotr\.git$ /lib/libotr.git last;
#  rewrite ^/git/otr_im_website\.git$ /infra/otr-im-website.git last;
#   rewrite ^/git/pidgin_otr.git/info/refs\?service=git-upload-pack$ /plugins/pidgin-otr.git/info/refs\?service=git-upload-pack last;


  location /git/libotr.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/lib/libotr.git;
  }

  location /git/otr_im_website.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/infra/otr-im-website.git;
  }

  location /git/pidgin_otr.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/plugins/pidgin-otr.git;
  }

  location /git/libotr-next.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/lib/libotr-next.git;
  }

  location / {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse;
  }

  subs_filter http://bugs.otr.im/ https://bugs.otr.im/ i;
  subs_filter "http://bugs.otr.im" "https://bugs.otr.im/" i;
  subs_filter 'http://bugs.otr.im' 'https://bugs.otr.im/' i;
  subs_filter http://moineau.otr.im/ https://moineau.otr.im/ i;
  subs_filter "http://moineau.otr.im" "https://moineau.otr.im/" i;
  subs_filter 'http://moineau.otr.im' 'https://moineau.otr.im/' i;


}

## onion
server {
  listen unix:/run/tor/nginx-onion-80.sock;
  server_name zmugvt6onw4mpu5w.onion;
  allow "unix:";
  deny all;
  server_tokens off; ## Don't show the nginx version number, a security best practice
  root /opt/gitlab/embedded/service/gitlab-rails/public;

  location = / {
    return 301 http://zmugvt6onw4mpu5w.onion/users/sign_in;
  }

  ## Individual nginx logs for this GitLab vhost
  access_log  /var/log/nginx/gitlab_onion_access.log;
  error_log   /var/log/nginx/gitlab_onion_error.log;

  location /git/libotr.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/lib/libotr.git;
  }

  location /git/otr_im_website.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/infra/otr-im-website.git;
  }

  location /git/pidgin_otr.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/plugins/pidgin-otr.git;
  }

  location /git/libotr-next.git {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse/lib/libotr-next.git;
  }

  location / {
    include /etc/nginx/snippets/gitlab.conf;
    proxy_pass http://gitlab-workhorse;
  }


  subs_filter http://bugs.otr.im/ http://zmugvt6onw4mpu5w.onion/ i;
  subs_filter https://bugs.otr.im/ http://zmugvt6onw4mpu5w.onion/ i;
  subs_filter "http://bugs.otr.im" "http://zmugvt6onw4mpu5w.onion" i;
  subs_filter "https://bugs.otr.im" "http://zmugvt6onw4mpu5w.onion" i;
  subs_filter 'http://bugs.otr.im' 'http://zmugvt6onw4mpu5w.onion' i;
  subs_filter 'https://bugs.otr.im' 'http://zmugvt6onw4mpu5w.onion' i;
  subs_filter http://moineau.otr.im/ http://zmugvt6onw4mpu5w.onion/ i;
  subs_filter https://moineau.otr.im/ http://zmugvt6onw4mpu5w.onion/ i;
  subs_filter "http://moineau.otr.im" "http://zmugvt6onw4mpu5w.onion" i;
  subs_filter "https://moineau.otr.im" "http://zmugvt6onw4mpu5w.onion" i;
  subs_filter 'http://moineau.otr.im' 'http://zmugvt6onw4mpu5w.onion' i;
  subs_filter 'https://moineau.otr.im' 'http://zmugvt6onw4mpu5w.onion' i;
  subs_filter https://zmugvt6onw4mpu5w.onion/ http://zmugvt6onw4mpu5w.onion/ i;
  subs_filter "https://zmugvt6onw4mpu5w.onion" "http://zmugvt6onw4mpu5w.onion" i;
  subs_filter 'https://zmugvt6onw4mpu5w.onion' 'http://zmugvt6onw4mpu5w.onion' i;

}
